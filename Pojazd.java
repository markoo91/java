
public class Pojazd extends Maszyna
{
   public static int maxLiczbaPojazdow;
   public int moc;
   public int momentObrotowy;
   public int nrPojazdu;
   Pojazd(String marka,String nazwa,int pojemnoscSilnika,String rodzajSilnika,int maxLiczbaPojazdow,int moc,int momentObrotowy,int nrPojazdu)
   {
     this.marka=marka;
     this.nazwa=nazwa;
     this.pojemnoscSilnika=pojemnoscSilnika;
     this.rodzajSilnika=rodzajSilnika;
     this.maxLiczbaPojazdow=maxLiczbaPojazdow;
     this.moc=moc;
     this.momentObrotowy=momentObrotowy;
     this.nrPojazdu=nrPojazdu;
   }
   public void wyswietl()
   {
   System.out.println("Pojazd wyprodukował : " + marka);
   System.out.println("Wabi sie : "+nazwa);
   System.out.println("Pojemnosc silnika to: "+pojemnoscSilnika);
   System.out.println("Rodzaj silnika: "+rodzajSilnika);
   System.out.println("Maksymalna liczba pojazdow: "+ maxLiczbaPojazdow);
   System.out.println("O mocy: "+moc);
   System.out.println("O momencie obrotowym: "+momentObrotowy);
   System.out.println("Numer pojazdu: "+ nrPojazdu);
   }
}
