import java.util.ArrayList;
public class ParkMaszynowy
{
public static void main(String [] args)
{
    ArrayList<Maszyna> maszyny = new ArrayList<Maszyna>();
    maszyny.add(new Pojazd("Honda","Quad 4x4",250,"benzyna",10,20,1000,1));
    maszyny.add(new Pojazd("Suzuki","Quad 4x4",500,"benzyna",2,40,2000,5));
    maszyny.add(new Kosiarka("Bosh","trawokos",50,"benzyna",true,true,3));
    maszyny.add(new Kosiarka("Sthil","trawokos superos",50,"benzyna",false,false,5));
    maszyny.add(new Samochod("Honda","Civic",1000,"benzyna",3,75,3000,8,"GE06","1111111154"));
    maszyny.add(new Samochod("Subaru","Impreza",2500,"benzyna",2,150,6600,1,"WRC","1546464"));
    maszyny.add(new Lokomotywa("Peso","Gabriel",5000,"diesel",false,10));
    maszyny.add(new Lokomotywa("Pendolino","Dżoszua",5000,"diesel",false,12));
    maszyny.add(new Jednoslad("Romet","Ogar",50,"benzyna",1,20,200,25,"3 biegowy"));
    maszyny.add(new Jednoslad("Motorynka",":D",50,"benzyna",1,20,200,25,"ach to dzieciństwo :P"));
    
    for(Maszyna m:maszyny)
    {
      if(m instanceof Kosiarka)
      {
          Kosiarka k1=new Kosiarka("Sthil","trawokos superos",50,"benzyna",false,false,5);
          m=k1;
          k1.setczyMelekser(true);
       }
        m.wyswietl();
        System.out.println();
    }
    System.out.println("W liscie znajduje sie "+maszyny.size()+ " maszyn ");
}
}