
public class Jednoslad extends Pojazd
{
 public String typ;
 Jednoslad(String marka,String nazwa,int pojemnoscSilnika,String rodzajSilnika,int maxLiczbaPojazdow,int moc,int momentObrotowy,int nrPojazdu,String typ)
 {
     super(marka,nazwa,pojemnoscSilnika,rodzajSilnika,maxLiczbaPojazdow,moc,momentObrotowy,nrPojazdu);
     this.typ=typ;
 }
 public void wyswietl()
 {
   System.out.println("Pojazd wyprodukował : " + marka);
   System.out.println("Wabi sie : "+nazwa);
   System.out.println("Pojemnosc silnika to: "+pojemnoscSilnika);
   System.out.println("Rodzaj silnika: "+rodzajSilnika);
   System.out.println("Maksymalna liczba pojazdow: "+ maxLiczbaPojazdow);
   System.out.println("O mocy: "+moc);
   System.out.println("O momencie obrotowym: "+momentObrotowy);
   System.out.println("Numer pojazdu: "+ nrPojazdu);
   System.out.println("Typ motocylku: "+typ);
}
}
