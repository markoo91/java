
public class Samochod extends Pojazd
{
public String model;
public String VIN;
Samochod(String marka,String nazwa,int pojemnoscSilnika,String rodzajSilnika,int maxLiczbaPojazdow,int moc,int momentObrotowy,int nrPojazdu,String model,String VIN )
{
    super(marka,nazwa,pojemnoscSilnika,rodzajSilnika,maxLiczbaPojazdow,moc,momentObrotowy,nrPojazdu);
    this.model=model;
    this.VIN=VIN;
  }    
public void wyswietl()
{
   System.out.println("Pojazd wyprodukował : " + marka);
   System.out.println("Wabi sie : "+nazwa);
   System.out.println("Pojemnosc silnika to: "+pojemnoscSilnika);
   System.out.println("Rodzaj silnika: "+rodzajSilnika);
   System.out.println("Maksymalna liczba pojazdow: "+ maxLiczbaPojazdow);
   System.out.println("O mocy: "+moc);
   System.out.println("O momencie obrotowym: "+momentObrotowy);
   System.out.println("Numer pojazdu: "+ nrPojazdu);
   System.out.println("Model samochodu : "+model);
   System.out.println("Numer VIN: "+VIN);
}
}
