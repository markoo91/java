
public class Lokomotywa extends Maszyna
{
   public boolean czyRadziecka;
   public int maxLiczbaWagonow;
   Lokomotywa(String marka,String nazwa,int pojemnoscSilnika,String rodzajSilnika,boolean czyRadziecka,int maxLiczbaWagonow)
   {
       this.marka=marka;
       this.nazwa=nazwa;
       this.pojemnoscSilnika=pojemnoscSilnika;
       this.rodzajSilnika=rodzajSilnika;
       this.czyRadziecka=czyRadziecka;
       this.maxLiczbaWagonow=maxLiczbaWagonow;
   }
   public void wyswietl()
   {
       System.out.println("Lokomotywa producenta : "+ marka);
       System.out.println("wołają na nią : "+ nazwa);
       System.out.println("ma moc silnika : " + pojemnoscSilnika);
       System.out.println("o rodzaju napędu : "+rodzajSilnika);
       System.out.println("Czy wyprodukowano ją w Soviet Union ? " + czyRadziecka);
       System.out.println("może mieć za sobą tyle wagonów: " + maxLiczbaWagonow);
   }
}